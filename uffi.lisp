;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-

;;; $Header: /project/cl-ipc/cvsroot/cl-ipc/uffi.lisp,v 1.1.1.1 2005/09/09 08:50:06 skamphausen Exp $
;;; $Id: uffi.lisp,v 1.1.1.1 2005/09/09 08:50:06 skamphausen Exp $

;;; Copyright (C) 2005 by
;;; Dr. Edmund Weitz http://weitz.de
;;; and
;;; Stefan Kamphausen http://www.skamphausen.de
;;; All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-ipc)

(defconstant +ipc-create-flag+ #o00001000)
(defconstant +ipc-exclusive-flag+ #o00002000)
(defconstant +ipc-rmid+ 0)
(defconstant +ipc-no-wait-flag+ #o4000)

;; def-struct from UFFI!
;; #. is reader macro to eval at read time
(def-struct msg-struct
    (mtype :long)
  (text (:array :char #.+message-length+)))

(def-foreign-type msg-struct-pointer (* msg-struct))

(def-function ("msgget" msg-get)
    ((key :int)
     (flag :int))
  :returning :int
  :module :cl-ipc)

(def-function ("msgrcv" msg-receive)
    ((id :int)
     (msg msg-struct-pointer)
     (msg-size :unsigned-int)
     (msg-type :long)
     (flag :int))
  :returning :int
  :module :cl-ipc)

(def-function ("msgctl" msg-control)
    ((id :int)
     (command :int)
     (msqid_ds :pointer-void))
  :returning :int
  :module :cl-ipc)

(defun msg-close (id)
  (ignore-errors
    (msg-control id +ipc-rmid+ (make-null-pointer :void))))


