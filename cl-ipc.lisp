;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: CL-USER; Base: 10 -*-

;;; $Header: /project/cl-ipc/cvsroot/cl-ipc/cl-ipc.lisp,v 1.3 2005/09/16 14:43:17 skamphausen Exp $
;;; $Id: cl-ipc.lisp,v 1.3 2005/09/16 14:43:17 skamphausen Exp $

;;; Copyright (C) 2005 by
;;; Dr. Edmund Weitz http://weitz.de
;;; and
;;; Stefan Kamphausen http://www.skamphausen.de
;;; All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-ipc)

(defmacro do-msg-queue ((var key &key nowait) &body body)
  "Loop forever over an IPC queue which will be opened using the
  number provided in KEY.  Within the body the passed variable VAR
  contains the current message from the client.

You can use EXIT-IPC-LOOP in the body to exit the loop.

A simple example:

 \(do-msg-queue (msg 2223 :nowait nil)
           (format t \"received ~A~%\" msg)
           (force-output)
           (when (string= msg \"quit\")
             (exit-ipc-loop \"optional argument\")))

If you set NOWAIT to t the body will be executed immediately and the
variable holding the message will be set to NIL.  In this case it is
up to you to wait or consume all CPU cycles.  The default is to only
execute the body when a message was received.
"
  (with-rebinding (key nowait)
    (with-unique-names (id ipc-loop ipc-result msg-buf msg-flag)
      `(let ((,id)
             (,msg-flag (if ,nowait +ipc-no-wait-flag+ 0)))
        (unwind-protect
          (progn
            (setf ,id (msg-get ,key
                               (logior +ipc-create-flag+
                                       +ipc-exclusive-flag+
                                       #o0666)))
            (unless (plusp ,id)
              (error "IPC GET ERROR (~a)" (get-unix-errno)))
            (block ,ipc-loop
              (flet ((exit-ipc-loop (&optional ,ipc-result)
                       (return-from ,ipc-loop ,ipc-result)))
                (with-foreign-object (,msg-buf 'msg-struct)
                  (loop
                    (let ((,var))
                      (if (minusp (msg-receive ,id ,msg-buf
                                               #.+message-length+
                                               1 ,msg-flag))
                        (if (and ,nowait (= (get-unix-errno) *err-no-message*))
                          (setf ,var nil)
                          (error "IPC RCV ERROR (~a)" (get-unix-errno)))
                        (setf ,var (convert-from-foreign-string
                                    (get-slot-value ,msg-buf 'msg-struct 'text))))
                      ,@body))))))
          (when (and ,id (plusp ,id))
            (msg-close ,id)))))))
