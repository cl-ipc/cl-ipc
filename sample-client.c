/* -*- C -*- */
/*
 * $Id: $
 *
 * Copyright (C) 2005 by Stefan Kamphausen <http://www.skamphausen.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * - Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * - Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials
 * provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Description:
 * A sample client implementation for use with IPC calls.
 */
# include <stdio.h>
# include <errno.h>
# include <string.h>
# include <unistd.h>
# include <stdlib.h>
# include <getopt.h>

# include <sys/types.h>
# include <sys/ipc.h>
# include <sys/msg.h>

#define STR_LEN 256
#define LINE_LEN 256

/* All messages get the same message type */
#define MSG_TYPE 1


/* This'll hold the data that's written into the queue */
struct msg_st {
    long int type;
    char line1[LINE_LEN];
};

/* What can happen after parsing the command lines */
enum operation_modes {
    CLEAN,
    WRITE
};

void usage(void) {
    printf("sample-client -k KEY -c      clean ipc queue with key KEY.\n");
    printf("sample-client -k KEY -p STR  Pass STR to the ipc line with key KEY.\n");
}

int get_message_queue(int key) {
    int msgid;

    if ((msgid = msgget((key_t) key, 0)) == -1) {
        fprintf(stderr, "msgget failed for key %d: %s\n",key,strerror(errno));
        exit(-5);
    } else {
        return msgid;
    }
}

void cleanup_stale_queue(int key) {
    int result;
    int msgid;
    
    printf("removing queue with key %d\n",key);
    msgid = get_message_queue(key);
    
    result = msgctl(msgid,IPC_RMID,NULL);
    
    if (result == -1) {
        fprintf(stderr,"Could not remove queue: %s\n",strerror(errno));
    }
    
}


void send_message_to_queue(int key, char* msg) {
    int msgid;
    struct msg_st queue_msg;

    printf("sending \"%s\" to queue with key %d\n",msg,key);
    msgid = get_message_queue(key);

    /* prepare data */
    snprintf(queue_msg.line1,STR_LEN, "%s", msg);
    queue_msg.type = MSG_TYPE;

    /* pass the data to the queue. */
    if (msgsnd(msgid, (void *) &queue_msg, sizeof(struct msg_st) - sizeof(long int), IPC_NOWAIT) == -1) {
        fprintf(stderr, "could not send message: %s\n",strerror(errno));
        exit(-6);
    }

}


int main(int argc, char *argv[]) {
    int mode = -1;
    int c;
    int this_option_optind;
    int option_index = 0;
    char opt_message[STR_LEN];
    int opt_key = -1;
    
    /* parse options */
    while (1) {
        this_option_optind = optind ? optind : 1;
        option_index = 0;
        /* no long options */
        static struct option long_options[] = {
            {0,0,0}
        };
        c = getopt_long(argc,argv,"ruwsp:ck:", long_options, &option_index);
        if (c == -1) {
            break;
        }
        switch(c) {
        case 0:
            /* printf("option %s\n",long_options[option_index].name); */
            break;
        case 'k':
            opt_key = strtol(optarg,NULL,10);
            break;
        case 'c':
            mode = CLEAN;
            break;
        case 'p':
            mode = WRITE;
            strncpy(opt_message,optarg,STR_LEN);
            break;
        default:
            printf("Can't handle option '%c'\n",c);
            usage();
            exit(-1);
        }
    }
    
    switch (mode) {
    case CLEAN:
        if (opt_key == -1) {
            fprintf(stderr,"no key given\n");
            usage();
            exit(-4);
        }
        cleanup_stale_queue(opt_key);
        break;
    case WRITE:
        if (opt_key == -1) {
            fprintf(stderr,"no key given\n");
            usage();
            exit(-4);
        }
        send_message_to_queue(opt_key,opt_message);
        break;
    }
    exit(0);
}
